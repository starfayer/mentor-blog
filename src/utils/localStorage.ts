import { createStore } from "solid-js/store";

export function checkIsLoggedIn(): boolean {
  const isLoggedIn = localStorage.getItem("isLoggedIn");
  switch (isLoggedIn) {
    case LoginStatus.loggedIn:
      return true;
    case LoginStatus.notLoggedIn:
      return false;
  }

  return false;
}
export function setIsLoggedIn(value: boolean): void {
  const isLogged = value ? LoginStatus.loggedIn : LoginStatus.notLoggedIn;
  localStorage.setItem("isLoggedIn", isLogged);
}




export function initStorage() {
  const isLogged = checkIsLoggedIn();
  if (!isLogged) {
    localStorage.setItem("isLoggedIn", LoginStatus.notLoggedIn);
  }
}

enum LoginStatus {
  loggedIn = "true",
  notLoggedIn = "false"
}

interface LocalStorage {
  login?: string,
  password?: string,
  isLogged: boolean
}

const [storage, setStorage] = createStore<LocalStorage>({
  login: undefined,
  password: undefined,
  isLogged: false,
});

export const useStorage = () => [storage, setStorage];