export async function connectDB(): Promise<Response> {
  const connectionStatus = await rFetch("/", {
    method: "GET",
    headers: {
      Accept: "application/json"
    },
  });
  return connectionStatus;
}

export async function checkUser(login: string, password: string): Promise<boolean> {
  const response = await rFetch("/checkUser", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({login, password})
  }).then(response => {
    switch (response.status) {
      case 200:
        return true;
      case 401:
      default:
        return false;
    }
  })
  return response;
}

export async function addUser(login: string, password: string) {
  const response = await rFetch("/addUser", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({login, password})
  }).then(response => {
    switch (response.status) {
      case 200:
        return true;
      case 401:
      default:
        return false;
    }
  })
  return response;
}
export async function addReview(name: string, email: string, reviewText: string) {
  const response = await rFetch("/addReview", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({name, email, text: reviewText})
  }).then(response => {
    switch (response.status) {
      case 200:
        return true;
      case 401:
      default:
        return false;
    }
  })
  return response;
}


const rFetch = (input: string, options?: RequestInit) => {
  const redirectINPUT = `${import.meta.env.VITE_BACK_URI}` + input;
  return fetch(redirectINPUT, options);
}
