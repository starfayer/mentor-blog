import { Component } from "solid-js";

interface ButtonProps {
  text: string
  class?: string
  onClick?: () => any
}
const SubmitButton: Component<ButtonProps> = (props) => {
  return (
      <button type="submit"
              class={`${props.class && props.class.split(" ")} border-none interactive-button bg-interactive rounded-md`}
              onClick={props.onClick}
      >
        {props.text}
      </button>
  )
}

export default SubmitButton;