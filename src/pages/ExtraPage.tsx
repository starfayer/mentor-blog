import type { Component } from 'solid-js';

import { A } from "@solidjs/router";
import { checkIsLoggedIn } from "../utils/localStorage";
import SubmitButton from "../components/SubmitButton";
import { createSignal } from "solid-js";
import { addReview } from "../utils/dbInterface";

const Extra: Component = () => {
  const isLoggedIn = checkIsLoggedIn();
  const [isLoading, setIsLoading] = createSignal(false);
  const [reviewProposal, setReviewProposal] = createSignal(false);

  function handleSubmit(e: SubmitEvent) {
    e.preventDefault();
    const target = e.target as HTMLFormElement;
    const [nameEl, mailEl, textEl] = target.elements;
    const [name, mail, text] =
        [(nameEl as HTMLInputElement).value, (mailEl as HTMLInputElement).value, (textEl as HTMLInputElement).value];

    if (!isLoading()) {
      setIsLoading(true);
      addReview(name, mail, text).then(handleResult);
    }
  }
  function handleResult(result: boolean) {
    if (result) {
      setReviewProposal(true);
      setIsLoading(false);
      setTimeout(() => setReviewProposal(false), 3000);
    }
  }

  const ErrorBlock = (
      <div class="wrapper gap-[20px] p-[40px]">
        <div>Прежде чем продолжить, пожалуйста, выполните вход</div>
        <A class="text-[black] no-underline" href="/login">
          <div class="interactive-button bg-interactive rounded-md">Войти</div>
        </A>
      </div>
  );
  const reviewProposalBlock = (
      <div>Ваш отзыв обязательно будет учтён!</div>
  )
  const loadingSpinnerBlock = (
      <div role="status">
        <svg aria-hidden="true" class="inline w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-yellow-400" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
          <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
        </svg>
        <span class="sr-only">Загрузка...</span>
      </div>
  )

  return isLoggedIn ? (
      <div class="w-[60%] wrapper p-[20px] gap-[15px]">
        <div>Давайте знакомится &#128075;</div>
        <div class="flex">
          <div class="self-center">Небольшой рассказ о том, как сделать анимацию "стикера" ►</div>
          <video controls class="w-[55%]">
            <source src="src/assets/video/mentor-lesson.mp4"/>
          </video>
        </div>
        <div class="flex flex-col md:flex-row w-[100%] gap-[30px] justify-between">
          <div class="flex flex-col gap-[10px] justify-center w-[100%] md:w-[55%]">
            <div>Хотите дать обратную связь? <br/>Напишите мне!</div>
            <form onSubmit={(e) => handleSubmit(e)} class="flex flex-col gap-[10px] text-base md:text-lg xl:text-xl">
              <div class="hint">
                <div>Ваше имя:</div>
                <input type="text" class="input w-full" name="firstName" placeholder="Ваше имя..." required />
              </div>
              <div class="hint">
                <div>Ваша почта (для ответа):</div>
                <input type="email" class="input w-full" name="gmail" placeholder="Ваш адрес электронной почты..." required />
              </div>
              <div class="hint">
                <div>Ваше предложение:</div>
                <textarea name="message" class="input w-full" required placeholder="Ваше сообщение..."></textarea>
              </div>
              <SubmitButton text={"Отправить"} class={"w-[100%]"}/>
            </form>
          </div>
          <div class="relative flex gap-[20px] flex-col items-center">
            <div class="text-center">Ознакомиться с кодом проекта можно по ссылке ниже</div>
            <a class="github-logo" href="https://github.com/">github.com</a>
            <div class="text-center">Спасибо за внимание!</div>
            <div class="absolute self-start bottom-1">
              {isLoading() ? loadingSpinnerBlock : reviewProposal() ? reviewProposalBlock : ""}
            </div>
          </div>
        </div>
      </div>
  ) : ErrorBlock;
}

export default Extra;