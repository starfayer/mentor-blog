import type { Component } from 'solid-js';
import currentPhoto from "../assets/current.jpg";
import schoolPhoto from "../assets/school.jpg";
import { A } from "@solidjs/router";
import SubmitButton from "../components/SubmitButton";

const Photo: Component<{ src: string }> = (props) => {
  return (
      <div class="flex items-center ">
        <img class="rounded-full w-[90px] sm:w-[120px] lg:w-[140px] xl:w-[180px] bg-no-repeat" src={props.src} alt="avatar"></img>
      </div>
  )
}
const Article: Component<{ text: string }> = (props) => {
  return (
      <div class="relative h-full">
        <div class="absolute w-full h-full text-gradient"></div>
        <p class="max-h-[180px] overflow-hidden m-0 w-auto overflow-ellipsis">
          {props.text}
        </p>
      </div>
  )
}

const firstArticle = "Привет! Меня зовут Никита и вот уже на протяжении нескольких лет я занимаюсь веб-разработкой. На этой странице " +
    "хотелось бы рассказать о том, каким образом я попал в программирование. Почему я занимаюсь именно тем, чем занимаюсь.";
const secondArticle = "Ещё со школы я начал увлекаться программированием. Как и многие мои сверстники мне нравилось играть в компьютер. " +
    "Всегда было интересно, каким образом эти картинки передвигаются по экрану. Закончив школу, я начал постепенно изучать верстку. " +
    "Как только разобрался с вёрсткой, занялся изучением JavaScript-а, а позже и других технологий. Путем постоянной работы я и дошел " +
    "до определенного уровня в фронтенд-разработке. Хотелось бы попробовать что-нибудь новое, но это уже будет другая история.";

const BaseContent: Component = () => {
  return (
      <div class="w-[45%] flex flex-col gap-[40px]">
        <div class="flex flex-col gap-[40px]">
          <div class="flex gap-x-[20px] justify-between items-center h-1/3">
            <Photo src={currentPhoto}/>
            <Article text={firstArticle}/>
          </div>
          <div class="flex gap-x-[20px] justify-between items-center h-1/3">
            <Article text={secondArticle}/>
            <Photo src={schoolPhoto}/>
          </div>
          <div class="flex"></div>
        </div>
        <div class="my-5 flex gap-y-[10px] flex-col items-center justify-center text-xl">
          <div class="text-center">Хотите узнать больше? Выполните вход</div>
          <A href="/login">
            <SubmitButton text={"Войти"} />
          </A>
        </div>
      </div>
  )
}

export default BaseContent;