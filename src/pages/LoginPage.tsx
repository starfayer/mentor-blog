import type { Component } from 'solid-js';
import { createSignal, Match, Switch } from "solid-js";
import { checkIsLoggedIn, setIsLoggedIn } from "../utils/localStorage";
import { A, useNavigate } from "@solidjs/router";
import SubmitButton from "../components/SubmitButton";
import { addUser, checkUser } from "../utils/dbInterface";

const LoginPage: Component = () => {
  const isLoggedIn = checkIsLoggedIn();

  const navigate = useNavigate();
  const [toAuthenticate, setToAuthenticate] = createSignal(!isLoggedIn);
  const [toRegister, setToRegister] = createSignal(false);
  const initialCorrect = false;
  const [dataError, setDataError] = createSignal(initialCorrect);
  const [isLoading, setIsLoading] = createSignal(initialCorrect);

  function changeBody(toRegister: boolean) {
    setToRegister(toRegister);
    setDataError(initialCorrect);
  }

  function handleResult(result: boolean) {
    if (result) {
      navigate("/extra", { replace: true });
      setIsLoggedIn(true);
    } else {
      setDataError(true);
    }
    setIsLoading(false);
  }

  function handleSubmit(e: SubmitEvent) {
    e.preventDefault();
    const target = e.target as HTMLFormElement;
    const [loginEl, passwordEl] = target.elements;
    const [login, password] = [(loginEl as HTMLInputElement).value, (passwordEl as HTMLInputElement).value];

    setDataError(false);
    if (!toRegister() && !isLoading()) {
      checkUser(login, password).then(handleResult);
    } else {
      addUser(login, password).then(handleResult);
    }
    setIsLoading(true);
  }

  const regProposal = (
      <div class="absolute bottom-2 text-center xl:text-xl sm:text-lg">
        Впервые здесь? Тогда <span class="underline cursor-pointer text-interactive hover:text-[blue]" onClick={() => changeBody(true)}>зарегистрируйтесь</span>
      </div>
  );
  const incorrectData = (
      <div class="visible peer-invalid:visible text-pink-600 text-sm">
        Ваши логин или пароль не совпадают. Проверьте корректность данных.
      </div>
  );
  const loadingSpinner = (
      <div role="status" class="mb-2 flex justify-center">
        <svg aria-hidden="true" class="inline w-8 h-8 mr-2 text-gray-200 animate-spin dark:text-gray-600 fill-yellow-400" viewBox="0 0 100 101" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z" fill="currentColor"/>
          <path d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z" fill="currentFill"/>
        </svg>
        <span class="sr-only">Загрузка...</span>
      </div>
  );
  return (
    <>
      {toAuthenticate() ? (
      <div class="relative w-[30%] h-2/4 py-1 gap-[20px] wrapper auth-proposal">
        <div class="text-center xl:leading-10 sm:leading-8">
          <Switch fallback={<div>FIX ME!!!</div>}>
            <Match when={toRegister()}>
              <div>здесь можно</div>
              <div>пройти регистрацию</div>
            </Match>
            <Match when={!toRegister()}>
              <div>войдите</div>
              <div>если понимаете зачем</div>
            </Match>
          </Switch>
        </div>
        <div class="flex flex-col sm:flex-row mb-3">
          <div class={`login-button rounded-s-3xl ${toRegister() ? "bg-active" : "bg-interactive"}`} onClick={() => changeBody(false)}>вход</div>
          <div class={`login-button rounded-e-3xl ${toRegister() ? "bg-interactive" : "bg-active"}`} onClick={() => changeBody(true)}>регистрация</div>
        </div>
        <form class="w-[180px] sm:w-[250px]" onSubmit={(e) => handleSubmit(e)}>
          {dataError() ? incorrectData : isLoading() ? loadingSpinner : ""}
          <div class="flex flex-col gap-[10px] mb-5">
            <input type="email" class={`input ${dataError() ? "invalid" : ""}`} placeholder="логин (e-mail)" name="login"
                   onChange={() => setDataError(false)} required/>
            <input type="password" class={`input ${dataError() ? "invalid" : ""}`} placeholder="пароль (ваш)"  name="password"
                   onChange={() => setDataError(false)} required/>
          </div>
          <SubmitButton text={toRegister() ? "Зарегистрироваться" : "Войти"} class={"w-[100%]"}/>
        </form>
        {toRegister() ? "" : regProposal}
      </div>
      ) : (
      <div class="w-[30%] gap-[20px] wrapper items-center p-10 auth-proposal">
        <div>Добро пожаловать!</div>
        <div class="flex gap-5 justify-between items-center">
          <div>Вы можете получить доступ к дополнительному контенту</div>
          <A href={"/extra"}>
            <SubmitButton text={"Контент"}/>
          </A>
        </div>
        <div class="flex gap-3 flex-col justify-around my-5 it  ems-center text-center">
          <div>Желаете войти под другим пользователем?</div>
          <SubmitButton text={"Вход"} onClick={() => setToAuthenticate(true)}/>
        </div>
      </div>
      )}
    </>
  )
}

export default LoginPage;