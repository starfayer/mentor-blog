import type { Component } from 'solid-js';
import { JSX } from "solid-js";
import { Route, Router, Routes, useIsRouting, } from "@solidjs/router";
import Login from "./pages/LoginPage";
import MainPage from "./pages/MainPage";
import ExtraPage from "./pages/ExtraPage";

const Transitioner: Component<{ children?: JSX.Element }> = (props) => {
  const isRouting = useIsRouting();
  return (
    <div class={`${isRouting() ? "fadeOut" : "fadeIn"}`}>
      {props.children}
    </div>
  )
}
const App: Component = () => {
  return (
    <Router>
      <Transitioner>
        <div class={`bg-background h-screen flex items-center justify-center`}>
          <Routes>
            <Route path="/" component={MainPage} />
            <Route path="/login" component={Login} />
            <Route path="/extra" component={ExtraPage} />
          </Routes>
        </div>
      </Transitioner>
    </Router>
  )
};

export default App;
