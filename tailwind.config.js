module.exports = {
    content: ["./src/**/*.{html,js,ts,tsx}", "./src/*.{html,js,ts,tsx}"],
    experimental: {
        optimizeUniversalDefaults: true,
    },
    theme: {
        extend: {
            colors: {
                background: "#FAF0D7",
                interactive: "#8CC0DE",
                hover: "#44B4F4",
                active: "#ffffff"
            },
        },
    },
    // have loaded base styles from preflight.css https://github.com/tailwindlabs/tailwindcss/blob/master/src/css/preflight.css
    corePlugins: {
        preflight: false
    },
    plugins: [],
}