const express = require("express");

const app = express();
app.use(express.json());
app.use(express.static("dist"));
app.listen(8001, () => {
    console.log(`App listening on port 8001`)
});
console.log(__dirname + '/dist/assets')
app.get("*", async function(req, res){
    console.log('Main page loading...');
    res.sendFile(__dirname + '/dist/index.html');
});

