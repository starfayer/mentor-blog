const router = require('express').Router();
const db = require("./db");

/** enable CORS */
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.get("/", async (req, res) => {
    const status = await db.connect();
    res.sendStatus(status);
});

router.post("/checkUser", async (req, res) => {
    const {login, password} = req.body;
    const userCorrect = await db.checkUser({login, password});
    if (userCorrect) {
        res.sendStatus(db.statusCode.response);
    } else {
        res.status(db.statusCode.dbOutError).send({
            errCode: db.statusCode.dbOutError,
            errMessage: "Such user is not defined."
        });
    }
});
router.post("/addReview", async (req, res) => {
    const {name, email, text} = req.body;
    const reviewAdded = await db.addReview({name, email, text});
    if (reviewAdded.acknowledged) {
        res.sendStatus(db.statusCode.response);
    } else {
        res.status(db.statusCode.dbOutError).send({
            errCode: db.statusCode.dbOutError,
            errMessage: "Review was not saved. Please check your request."
        });
    }
});

router.post("/addUser", async (req, res) => {
    const {login, password} = req.body;
    const userExists = await db.checkUser({login, password});
    if (userExists) {
        res.status(db.statusCode.dbOutError).send({
            errCode: db.statusCode.dbOutError,
            errMessage: "User has already been existed. Check your input."
        });
        return false;
    }

    const userAdded = await db.addUser({login, password});
    if (userAdded.acknowledged) {
        res.sendStatus(db.statusCode.response);
    } else {
        res.status(db.statusCode.dbOutError).send({
            errCode: db.statusCode.dbOutError,
            errMessage: "User was not saved. Check your input."
        });
    }
    return userAdded;
});

module.exports = router;