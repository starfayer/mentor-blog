const { MongoClient } = require("mongodb");
const bcrypt = require("bcryptjs");

const dbModule = Object.create(null);
dbModule.connect = async function (uri) {
    const dbURI = uri || process.env.DB_URI;
    if (!dbModule.client) {
        dbModule.client = new MongoClient(dbURI, {
            serverSelectionTimeoutMS: process.env.TIMEOUT
        });
    }
    return await dbModule.client.connect()
        .then((res) => {
            dbModule.connected = true;
            console.log("Connection established");
            const database = dbModule.client.db("local");
            dbModule.collection = database.collection("users");
            dbModule.reviewCollection = database.collection("reviews");
            return statusCode.response;
        })
        .catch((err) => {
            dbModule.connected = false;
            console.error("Connection to DB was refused. Error data:\n", err);
            return statusCode.timeout;
        });
}

dbModule.getUser = async function (query) {
    return await dbModule.collection.findOne(query);
}
dbModule.checkUser = async function (query) {
    if (!dbModule.connected) {
        await dbModule.connect();
    }

    const userData = await dbModule.getUser({login: query.login});
    if (userData) {
         return checkHashedData(query.password, userData.password);
    } else {
        return false;
    }
}
dbModule.addUser = async function (query) {
    if (!dbModule.connected) {
        await dbModule.connect();
    }
    query.password = hashData(query.password);

    return await dbModule.collection.insertOne(query);
}
dbModule.addReview = async function (query) {
    if (!dbModule.connected) {
        await dbModule.connect();
    }
    return await dbModule.reviewCollection.insertOne(query);
}

const numSaltRounds = 10;
function hashData(data) {
    return bcrypt.hashSync(data, numSaltRounds);
}
function checkHashedData(data, hashedData) {
    return bcrypt.compare(data, hashedData);
}

const statusCode = Object.create(null);
statusCode.response = 200;
statusCode.timeout = 408;
statusCode.dbOutError = 401;
dbModule.statusCode = statusCode;

module.exports = dbModule;