const express = require("express");
require("dotenv").config();
const router = require("./router");

const app = express();
app.use(express.json());
app.use("/db", router);
app.listen(process.env.BACK_PORT, () => {
    console.log(`App listening on port ${process.env.BACK_PORT}`)
});